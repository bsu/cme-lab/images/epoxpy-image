FROM registry.gitlab.com/bsu/cme-lab/images/base-image:latest


RUN git clone https://bitbucket.org/cmelab/epoxpy.git \
    && cd epoxpy \
    && git checkout v2.1.0 \
    && conda install -c conda-forge -c omnia -c mosdef --file requirements.txt \
    && pip install -e . \
    && pytest
